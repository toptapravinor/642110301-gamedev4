using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class StartButtonHandler : MonoBehaviour
{
    void Start()
    {
        UIDocument uiDocument = GetComponent<UIDocument>();

        if (uiDocument != null)
        {
            VisualElement startButton = uiDocument.rootVisualElement.Q("startButton");
            VisualElement optionButton = uiDocument.rootVisualElement.Q("optionButton");
            VisualElement menuButton = uiDocument.rootVisualElement.Q("menuButton");

            // เพิ่มตรวจสอบและ Debug.Log สำหรับ VisualElement ที่ต้องการ
            if (startButton != null)
            {
                Debug.Log("Found startButton VisualElement!");
                startButton.RegisterCallback<ClickEvent>(OnStartButtonClicked);
            }
            else
            {
                Debug.LogError("startButton VisualElement not found in UI Toolkit.");
            }

            if (optionButton != null)
            {
                Debug.Log("Found optionButton VisualElement!");
                optionButton.RegisterCallback<ClickEvent>(OnOptionButtonClicked);
            }
            else
            {
                Debug.LogError("optionButton VisualElement not found in UI Toolkit.");
            }

            if (menuButton != null)
            {
                Debug.Log("Found menuButton VisualElement!");
                menuButton.RegisterCallback<ClickEvent>(OnMenuButtonClicked);
            }
            else
            {
                Debug.LogError("menuButton VisualElement not found in UI Toolkit.");
            }
        }
        else
        {
            Debug.LogError("UIDocument component not found.");
        }
    }

    void OnStartButtonClicked(ClickEvent evt)
    {
        Debug.Log("Start button clicked!");
        SceneManager.LoadScene("GamePlayScene");
    }

    void OnOptionButtonClicked(ClickEvent evt)
    {
        Debug.Log("Option button clicked!");
        SceneManager.LoadScene("OptionScene");
    }

    void OnMenuButtonClicked(ClickEvent evt)
    {
        Debug.Log("Menu button clicked!");
        SceneManager.LoadScene("MenuScene");
    }
}
